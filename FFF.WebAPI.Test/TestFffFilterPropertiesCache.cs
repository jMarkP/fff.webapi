﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFF.WebApi.Core.Compile;
using NUnit.Framework;

namespace FFF.WebAPI.Test
{
    [TestFixture]
    public class TestFffFilterPropertiesCache
    {
        [Test]
        public void CanGetProperties()
        {
            var result = FffFilterPropertiesCache.GetProperties(typeof (TestClassParent));


        }


        private class TestClassParent
        {
            public string Name { get; set; }
            public bool Flag { get; set; }
            public int Count { get; set; }
            public bool? PossibleFlag { get; set; }

            public TestClassChild Child { get; set; }
        }

        private class TestClassChild
        {
            public string Name { get; set; }
            public bool Flag { get; set; }
            public int Count { get; set; }
            public bool? PossibleFlag { get; set; }
        }
    }
}
