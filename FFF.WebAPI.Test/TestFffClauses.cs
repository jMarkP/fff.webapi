﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFF.WebApi.Core;
using FFF.WebApi.Core.Clauses;
using NUnit.Framework;

namespace FFF.WebAPI.Test
{
    //[TestFixture]
    //public class TestFffClauses
    //{
    //    [Test]
    //    public void CanBuildEqualsClause()
    //    {
    //        var SUT = new FffSinglePropertyClause<TestItem>("A", "=", "One");
    //        var SUTNeg = new FffSinglePropertyClause<TestItem>("A", "<>", "One");

    //        var testPass1 = new TestItem
    //        {
    //            A = "One"
    //        };
    //        var testPass2 = new TestItem
    //        {
    //            A = "one"
    //        };
    //        var testFail1 = new TestItem
    //        {
    //            A = "Two"
    //        };

    //        Assert.IsTrue(SUT.Accept(testPass1));
    //        Assert.IsTrue(SUT.Accept(testPass2));
    //        Assert.IsFalse(SUT.Accept(testFail1));

    //        Assert.IsFalse(SUTNeg.Accept(testPass1));
    //        Assert.IsFalse(SUTNeg.Accept(testPass2));
    //        Assert.IsTrue(SUTNeg.Accept(testFail1));
    //    }

    //    [Test]
    //    public void CanBuildRegexClause()
    //    {
    //        var SUT = new FffSinglePropertyClause<TestItem>("A", "~", "One.*");
    //        var SUTNeg = new FffSinglePropertyClause<TestItem>("A", "!~", "One.*");

    //        var testPass1 = new TestItem
    //        {
    //            A = "One"
    //        };
    //        var testPass2 = new TestItem
    //        {
    //            A = "one_Extra"
    //        };
    //        var testFail1 = new TestItem
    //        {
    //            A = "On"
    //        };

    //        Assert.IsTrue(SUT.Accept(testPass1));
    //        Assert.IsTrue(SUT.Accept(testPass2));
    //        Assert.IsFalse(SUT.Accept(testFail1));

    //        Assert.IsFalse(SUTNeg.Accept(testPass1));
    //        Assert.IsFalse(SUTNeg.Accept(testPass2));
    //        Assert.IsTrue(SUTNeg.Accept(testFail1));
    //    }

    //    private class TestItem
    //    {
    //        public string A { get; set; }
    //        public bool Flag { get; set; }
    //    }
    //}
}
