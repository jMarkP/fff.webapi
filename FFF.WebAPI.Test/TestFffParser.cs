using FFF.WebApi.Core.Compile;
using NUnit.Framework;

namespace FFF.WebAPI.Test
{
    [TestFixture]
    public class TestFffParser
    {
        //[Test]
        //public void CanParse()
        //{
        //    var input = "a ~ one.* AND flag = true";

        //    var SUT = new FffCompiler(new FffFilterPropertiesCache());

        //    var result = SUT.Parse<TestItem>(input);

        //    var items = new[]
        //    {
        //        new TestItem {A = "one", Flag = true}
        //    };

        //    var filtered = result.Filter(items);
        //}

        [Test]
        public void CanParseCorrectly()
        {
            var SUT = new FffParser<TestItem>();

            var input = "a ~ one[.*] AND flag =true";
            var result = SUT.ParseQuery(input);
        }


        private class TestItem
        {
            public string A { get; set; }
            public bool Flag { get; set; }
        }
    }
}