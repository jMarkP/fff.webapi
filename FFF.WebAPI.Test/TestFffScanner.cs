﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFF.WebApi.Core.Compile;
using NUnit.Framework;

namespace FFF.WebAPI.Test
{
    [TestFixture]
    class TestFffScanner
    {
        [TestCase("")]
        [TestCase(null)]
        [TestCase("   ")]
        [TestCase("\t\n")]
        public void HandlesNullAndEmpty(string input)
        {
            var SUT = new FffScanner(input, true, true);
            var token = SUT.Next();
            Assert.AreEqual(FffTokenKind.EOL, token.Kind);
            Assert.IsNull(token.Value);
        }

        [TestCase("one", "one")]
        [TestCase("one AND two", "one", "AND", "two")]
        [TestCase("one   AND\ttwo", "one", "AND", "two")]
        [TestCase("one&(two=three)", "one", "&", "(", "two", "=", "three", ")")]
        [TestCase("'this !=will &just be (one token)'", "this !=will &just be (one token)")]
        [TestCase("'this !=will &just be (one token)", "this !=will &just be (one token)")]
        [TestCase("one='something in &(quotes~)' two !='hello' a", "one", "=", "something in &(quotes~)", "two", "!=", "hello", "a")]
        [TestCase("=one&two AND three = '' ~(a b c)", "=", "one", "&", "two", "AND", "three", "=", "", "~", "(", "a", "b", "c", ")")]
        public void SplitsCorrectly(string input, params string[] expected)
        {
            var SUT = new FffScanner(input, true, true);

            var result = new List<string>();
            var token = SUT.Next();
            while (token.Kind != FffTokenKind.EOL)
            {
                result.Add(token.Value);
                token = SUT.Next();
            }
            AssertTokensMatch(expected, result);
        }

        [TestCase("one&(two=three)", 
            FffTokenKind.Text, FffTokenKind.BooleanOperator, FffTokenKind.CollectionStart, FffTokenKind.Text, FffTokenKind.FieldOperator, FffTokenKind.Text, FffTokenKind.CollectionEnd)]
        public void GeneratesCorrectTokenTypes(string input, params FffTokenKind[] expected)
        {
            var SUT = new FffScanner(input, true, true);

            var result = new List<FffTokenKind>();
            var token = SUT.Next();
            while (token.Kind != FffTokenKind.EOL)
            {
                result.Add(token.Kind);
                token = SUT.Next();
            }
            AssertTokenKindsMatch(expected, result);
        }

        private void AssertTokensMatch(IEnumerable<string> expected, IEnumerable<string> actual)
        {
            Assert.AreEqual(expected.Count(), actual.Count(), "Different number of tokens generated: [{0}] != [{1}]",
                WriteTokens(expected), WriteTokens(actual));

            for (var i = 0; i < expected.Count(); i++)
            {
                Assert.AreEqual(expected.ElementAt(i), actual.ElementAt(i), "Tokens at index {0} don't match: [{0}] != [{1}]",
                    WriteTokens(expected), WriteTokens(actual));
            }
        }

        private void AssertTokenKindsMatch(IEnumerable<FffTokenKind> expected, IEnumerable<FffTokenKind> actual)
        {
            Assert.AreEqual(expected.Count(), actual.Count(), "Different number of tokens generated: [{0}] != [{1}]",
                WriteTokens(expected), WriteTokens(actual));

            for (var i = 0; i < expected.Count(); i++)
            {
                Assert.AreEqual(expected.ElementAt(i), actual.ElementAt(i), "Tokens at index {0} don't match: [{0}] != [{1}]",
                    WriteTokens(expected), WriteTokens(actual));
            }
        }

        private string WriteTokens(IEnumerable<string> tokens)
        {
            return string.Join(", ", tokens.Select(t => "'" + t + "'"));
        }

        private string WriteTokens(IEnumerable<FffTokenKind> tokens)
        {
            return string.Join(", ", tokens);
        }
    }
}
