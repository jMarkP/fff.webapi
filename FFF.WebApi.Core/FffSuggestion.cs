﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFF.WebApi.Core.Compile;

namespace FFF.WebApi.Core
{
    public class FffSuggestion
    {
        private readonly string value;

        public FffSuggestion(string value)
        {
            this.value = value;
        }

        public string Value
        {
            get { return value; }
        }
    }
}
