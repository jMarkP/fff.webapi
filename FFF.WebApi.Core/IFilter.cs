﻿namespace FFF.WebApi.Core
{
    public interface IFilter<TItem>
    {
        bool Accept(TItem item);
    }
}