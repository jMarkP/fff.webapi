using System;

namespace FFF.WebApi.Core
{
    public class FffCompileException : Exception
    {
        public FffCompileException(string message): base(message)
        {
            
        }   
    }
}