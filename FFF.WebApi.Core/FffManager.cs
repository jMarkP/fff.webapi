﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFF.WebApi.Core.Compile;

namespace FFF.WebApi.Core
{
    public class FffManager
    {
        public IEnumerable<FffSuggestion> GetSuggestions<TItem>(string query, int cursorPos, int maxResults, IEnumerable<TItem> source)
        {
            var parser = new FffParser<TItem>();

            var properties = FffFilterPropertiesCache.GetProperties(typeof(TItem));

            var clauses = parser.ParseQuery(query);

            var clauseAtCursor = clauses.FirstOrDefault(c => c.ContainsCursor(cursorPos));

            if (clauseAtCursor == null)
                return new FffSuggestion[0];

            var annotatedToken = clauseAtCursor.GetTokenAtPos(cursorPos);

            switch (annotatedToken.Kind)
            {
                case FffAnnotatedTokenKind.Punctuation:
                    return new FffSuggestion[0];
                case FffAnnotatedTokenKind.ReservedWord:
                    return annotatedToken.ValidReservedWords.Select(rw => new FffSuggestion(rw)).Take(maxResults);
                case FffAnnotatedTokenKind.PropertyName:
                    return properties.Select(p => new FffSuggestion(p.DisplayName)).Take(maxResults);
                case FffAnnotatedTokenKind.PropertyValue:
                    if (annotatedToken.LimitedToProperty)
                    {
                        return GetSuggestionsForProperty(properties, annotatedToken.PropertyToLimitTo,
                            annotatedToken.Token.Value, source, maxResults);
                    }
                    else
                        return GetFreeTextSuggestions(properties, annotatedToken.Token.Value, source, maxResults);
                default:
                    throw new ArgumentOutOfRangeException();
            }

        }

        private IEnumerable<FffSuggestion> GetSuggestionsForProperty<TItem>(FffFilterProperty[] properties, string propertyToLimitTo, string value, IEnumerable<TItem> source, int maxResults)
        {
            var property =
                properties.FirstOrDefault(
                    p => p.DisplayName.Equals(propertyToLimitTo, StringComparison.InvariantCultureIgnoreCase));

            if (property == null)
                return new FffSuggestion[0];

            return source.SelectMany(x => property.GetValue(x))
                .Where(x => value.Equals(x, StringComparison.InvariantCultureIgnoreCase))
                .Distinct()
                .Select(x => new FffSuggestion(x))
                .Take(maxResults);
        }


        private IEnumerable<FffSuggestion> GetFreeTextSuggestions<TItem>(FffFilterProperty[] properties, string value, IEnumerable<TItem> source, int maxResults)
        {
            return source.SelectMany(x => properties.SelectMany(p => p.GetValue(x)))
                .Where(x => value.Equals(x, StringComparison.InvariantCultureIgnoreCase))
                .Distinct()
                .Select(x => new FffSuggestion(x))
                .Take(maxResults);
        }
    }
}
