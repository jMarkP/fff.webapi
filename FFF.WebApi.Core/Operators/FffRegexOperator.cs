﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace FFF.WebApi.Core.Operators
{
    public class FffRegexOperator : FffOperator
    {
        private readonly bool ignoreCase;
        private Regex regex;
        public FffRegexOperator( bool negate, bool ignoreCase)
            : base(negate)
        {
            this.ignoreCase = ignoreCase;
        }

        public override void Bind(string value)
        {
            base.Bind(value);
            regex = new Regex(value, ignoreCase ? RegexOptions.IgnoreCase : RegexOptions.None);
        }

        public override bool Action(IEnumerable<string> operands)
        {
            base.Action(operands);
            var rawResult = operands.Any(operand => regex.IsMatch(operand));
            return negate ? !rawResult : rawResult;
        }
    }
}