﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FFF.WebApi.Core.Operators
{
    public class FffEqualsOperator : FffOperator
    {
        private readonly bool ignoreCase;

        public FffEqualsOperator(bool negate, bool ignoreCase)
            : base(negate)
        {
            this.ignoreCase = ignoreCase;
        }

        public override bool Action(IEnumerable<string> operands)
        {
            base.Action(operands);
            var rawResult = operands.Any(operand => string.Equals(value, operand,
                ignoreCase ? StringComparison.InvariantCultureIgnoreCase : StringComparison.InvariantCulture));
            return negate
                ? !rawResult
                : rawResult;
        }
    }
}