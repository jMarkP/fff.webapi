namespace FFF.WebApi.Core.Operators
{
    public static class OperatorFactory
    {
        public static FffOperator Build(string op)
        {
            switch (op)
            {
                case "=":
                    return new FffEqualsOperator(false, true);
                case "!=":
                    return new FffEqualsOperator(true, true);
                case "~":
                    return new FffRegexOperator(false, true);
                case "!~":
                    return new FffRegexOperator(true, true);
                // TODO: in operator
                default:
                    throw new FffCompileException("Unsupported operator: " + op);
            }
        }
    }
}