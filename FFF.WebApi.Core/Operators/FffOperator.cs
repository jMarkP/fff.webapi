using System.Collections.Generic;

namespace FFF.WebApi.Core.Operators
{
    public abstract class FffOperator
    {
        protected string value;
        protected readonly bool negate;
        protected bool isBound;

        protected FffOperator(bool negate)
        {
            this.negate = negate;
        }

        public virtual void Bind(string value)
        {
            isBound = true;
            this.value = value;
        }

        public virtual bool Action(IEnumerable<string> operands)
        {
            if (!isBound)
                throw new FffCompileException("Operator not bound to value");
            return true;
        }
    }
}