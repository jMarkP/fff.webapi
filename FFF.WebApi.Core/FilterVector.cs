﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFF.WebApi.Core
{
    /// <summary>
    /// Simple way to filter a large collection of items repeatedly.
    /// Uses a sieve to efficiently record which items to include
    /// </summary>
    /// <typeparam name="TItem"></typeparam>
    public class FilterVector<TItem> : IEnumerable<TItem>
    {
        private readonly TItem[] unfiltered;
        private readonly bool[] sieve;

        public FilterVector(IEnumerable<TItem> source)
        {
            var unfilteredArray = source as TItem[];
            if (unfilteredArray == null)
                unfilteredArray = source.ToArray();
            unfiltered = unfilteredArray;
            sieve = new bool[unfiltered.Length];
            for (int i = 0; i < sieve.Length; i++)
            {
                sieve[i] = true;
            }
        }

        public void Apply(IFilter<TItem> filter)
        {
            for (var i = 0; i < unfiltered.Length; i++)
            {
                if (!sieve[i])
                    continue;
                if (!filter.Accept(unfiltered[i]))
                {
                    sieve[i] = false;
                }
            }
        }


        public IEnumerator<TItem> GetEnumerator()
        {
            for (var i = 0; i < unfiltered.Length; i++)
            {
                if (sieve[i])
                    yield return unfiltered[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
