﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFF.WebApi.Core.Compile;

namespace FFF.WebApi.Core.AST
{
    public abstract class BaseSyntaxNode<TItem>
    {
        private readonly FffAnnotatedToken[] tokens;

        protected BaseSyntaxNode(params FffAnnotatedToken[] tokens)
        {
            this.tokens = tokens;
        }

        public bool ContainsCursor(int cursorPos)
        {
            return tokens.Any(t => t.Token.ContainsCursor(cursorPos));
        }

        public FffAnnotatedToken GetTokenAtPos(int cursorPos)
        {
            return tokens.FirstOrDefault(t => t.Token.ContainsCursor(cursorPos));
        }

        public IEnumerable<FffSuggestion> GetSuggestions(IEnumerable<TItem> source, int cursorPos, int? maxToReturn)
        {
            var all = CalculateSuggestions(source, cursorPos);
            if (maxToReturn.HasValue)
                return all.Take(maxToReturn.Value);
            return all;
        }

        protected abstract IEnumerable<FffSuggestion> CalculateSuggestions(IEnumerable<TItem> source, int cursorPos);

        public virtual bool IsValid()
        {
            return tokens.All(t => t.Token.Kind != FffTokenKind.Artificial);
        }
    }
}
