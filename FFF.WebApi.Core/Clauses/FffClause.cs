﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using FFF.WebApi.Core.AST;
using FFF.WebApi.Core.Compile;

namespace FFF.WebApi.Core.Clauses
{
    /// <summary>
    /// Single clause in an FFF filter
    /// </summary>
    public abstract class FffClause<TItem> : BaseSyntaxNode<TItem>, IFilter<TItem>
    {
        private Func<FffClause<TItem>, TItem, bool> includeFunc;
        protected static FffFilterProperty[] fields;

        static FffClause()
        {
            fields = FffFilterPropertiesCache.GetProperties(typeof(TItem));
        } 

        protected FffClause(params FffAnnotatedToken[] tokens)
            :base(tokens)
        {
        }

        protected abstract bool IncludeFunc(TItem item);
        
        public bool Accept(TItem item)
        {
            return IncludeFunc(item);
        }
    }

}
