using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using FFF.WebApi.Core.Compile;
using FFF.WebApi.Core.Operators;

namespace FFF.WebApi.Core.Clauses
{
    public class FffFreeTextClause<TItem> : FffClause<TItem>
    {
        private readonly FffFilterProperty[] freeTextProperties;
        private readonly bool regexMode;
        private readonly string value;

        private static readonly HashSet<char> RegexChars = new HashSet<char>(new[]
        {
            '^', '$', '*', '+', '|', '?'
        });

        private Regex regex;

        public FffFreeTextClause(FffToken textToken) : base (FffAnnotatedToken.PropertyValue(textToken, false, null))
        {
            this.freeTextProperties = fields;
            this.value = textToken.Value;
            this.regexMode = value.Any(c => RegexChars.Contains(c));
            if (this.regexMode)
                this.regex = new Regex(value, RegexOptions.IgnoreCase);
        }

        protected override bool IncludeFunc(TItem item)
        {
            return freeTextProperties.Any(p => p.GetValue(item).Any(Matches));
        }

        private bool Matches(string target)
        {
            if (regexMode)
                return regex.IsMatch(target);
            return target.Contains(value);
        }

        protected override IEnumerable<FffSuggestion> CalculateSuggestions(IEnumerable<TItem> source, int cursorPos)
        {
            throw new NotImplementedException();
        }
    }

    public class FffSinglePropertyClause<TItem> : FffClause<TItem>
    {
        private readonly FffFilterProperty property;
        private readonly FffOperator op;

        public FffSinglePropertyClause(FffToken property, FffToken op, FffToken value) 
            : base(FffAnnotatedToken.PropertyName(property),
                  FffAnnotatedToken.ReservedWord(op, FffConstants.SingleFieldOperators),
                  FffAnnotatedToken.PropertyValue(value, true, property.Value))
        {
            this.property = fields.FirstOrDefault(f => f.DisplayName.Equals(property.Value, StringComparison.InvariantCultureIgnoreCase));
            this.op = OperatorFactory.Build(op.Value);
            this.op.Bind(value.Value);
        }

        protected override bool IncludeFunc(TItem item)
        {
            var targetValue = property.GetValue(item);
            return op.Action(targetValue);
        }

        protected override IEnumerable<FffSuggestion> CalculateSuggestions(IEnumerable<TItem> source, int cursorPos)
        {
            throw new NotImplementedException();
        }
    }
}