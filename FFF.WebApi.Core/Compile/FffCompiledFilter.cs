using System.Collections.Generic;
using System.Linq;

namespace FFF.WebApi.Core.Compile
{
    public class FffCompiledFilter<TItem>
    {
        private readonly IFilter<TItem>[] filters;

        public FffCompiledFilter(IEnumerable<IFilter<TItem>> filters)
        {
            this.filters = filters.ToArray();
        }

        public IEnumerable<TItem> Filter(IEnumerable<TItem> source)
        {
            var vector = new FilterVector<TItem>(source);

            foreach (var filter in filters)
            {
                vector.Apply(filter);
            }

            return vector;
        } 
    }
}