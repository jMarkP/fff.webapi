﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using FFF.WebApi.Core.Clauses;
//using FFF.WebApi.Core.Operators;

//namespace FFF.WebApi.Core.Compile
//{
//    public class FffCompiler
//    {
//        private readonly FffFilterPropertiesCache propertiesCache;

//        public FffCompiler(FffFilterPropertiesCache propertiesCache)
//        {
//            this.propertiesCache = propertiesCache;
//        }

//        public FffCompiledFilter<TItem> Parse<TItem>(string filterString)
//        {
//            // 1. Lexer
//            var tokens = GetTokens(filterString);

//            // 2. Parser
//            var clauses = Parse<TItem>(tokens);

//            return new FffCompiledFilter<TItem>(clauses);
//        }

//        private IEnumerable<string> GetTokens(string filterString)
//        {
//            return filterString.Split(' ', '\t');
//        }

//        private enum ParserState
//        {
//            PropertyOrText,
//            Operator,
//            Value,
//            Join
//        }

//        private IEnumerable<IFilter<TItem>> Parse<TItem>(IEnumerable<string> tokens)
//        {
//            var state = ParserState.PropertyOrText;

//            var properties = propertiesCache.GetProperties(typeof (TItem));

//            FffFilterProperty currentProperty = null;
//            FffOperator currentOperator = null;
//            string currentValue = null;
//            foreach (var token in tokens)
//            {
//                // For now just throw exceptions if things aren't exactly <Property Operator Value> tuples,
//                // joined by AND
//                switch (state)
//                {
//                    case ParserState.PropertyOrText:
//                        var property = properties.FirstOrDefault(p => p.DisplayName.Equals(token, StringComparison.InvariantCultureIgnoreCase));
//                        if (property == null)
//                        {
//                            yield return new FffFreeTextClause<TItem>(properties, token);
//                            state = ParserState.Join;
//                        }
//                        else
//                        {
//                            currentProperty = property;
//                            state = ParserState.Operator;
//                        }
//                        break;
//                    case ParserState.Operator:
//                        currentOperator = OperatorFactory.Build(token);
//                        state = ParserState.Value;
//                        break;
//                    case ParserState.Value:
//                        currentValue = token;
//                        yield return new FffSinglePropertyClause<TItem>(currentProperty, currentOperator, currentValue);
//                        state = ParserState.Join;
//                        break;
//                    case ParserState.Join:
//                        if (!string.Equals(token, "AND", StringComparison.InvariantCultureIgnoreCase))
//                            throw new FffCompileException("Syntax error, expected 'AND'");
//                        state = ParserState.PropertyOrText;
//                        break;
//                    default:
//                        throw new ArgumentOutOfRangeException();
//                }
//            }
//        }
//    }
//}
