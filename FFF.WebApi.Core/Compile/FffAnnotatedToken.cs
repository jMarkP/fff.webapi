namespace FFF.WebApi.Core.Compile
{
    public class FffAnnotatedToken
    {
        private readonly FffToken token;
        private readonly FffAnnotatedTokenKind kind;

        // For ReservedWord kind
        private readonly string[] validReservedWords;
        
        // For PropertyValue kind
        private readonly bool limitedToProperty;
        private readonly string propertyToLimitTo;

        public FffToken Token
        {
            get { return token; }
        }

        public FffAnnotatedTokenKind Kind
        {
            get { return kind; }
        }

        public string[] ValidReservedWords
        {
            get { return validReservedWords; }
        }

        public bool LimitedToProperty
        {
            get { return limitedToProperty; }
        }

        public string PropertyToLimitTo
        {
            get { return propertyToLimitTo; }
        }

        public static FffAnnotatedToken Punctuation(FffToken token)
        {
            return new FffAnnotatedToken(token, FffAnnotatedTokenKind.Punctuation, null, false, null);
        }

        public static FffAnnotatedToken ReservedWord(FffToken token, params string[] validReservedWords)
        {
            return new FffAnnotatedToken(token, FffAnnotatedTokenKind.ReservedWord, validReservedWords, false, null);
        }

        public static FffAnnotatedToken PropertyName(FffToken token)
        {
            return new FffAnnotatedToken(token, FffAnnotatedTokenKind.PropertyName, null, false, null);
        }

        public static FffAnnotatedToken PropertyValue(FffToken token, bool limitedToProperty, string propertyToLimitTo)
        {
            return new FffAnnotatedToken(token, FffAnnotatedTokenKind.PropertyValue, null, limitedToProperty, propertyToLimitTo);
        }

        private FffAnnotatedToken(FffToken token, FffAnnotatedTokenKind kind, string[] validReservedWords, bool limitedToProperty, string propertyToLimitTo)
        {
            this.token = token;
            this.kind = kind;
            this.validReservedWords = validReservedWords;
            this.limitedToProperty = limitedToProperty;
            this.propertyToLimitTo = propertyToLimitTo;
        }
    }
}