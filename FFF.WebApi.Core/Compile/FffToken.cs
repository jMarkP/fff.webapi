namespace FFF.WebApi.Core.Compile
{
    public class FffToken
    {
        private readonly FffTokenKind kind;
        private readonly string value;
        private readonly int startPos;
        private readonly int endPos;

        public FffToken(FffTokenKind kind, string value, int startPos, int endPos)
        {
            this.kind = kind;
            this.value = value;
            this.startPos = startPos;
            this.endPos = endPos;
        }

        public FffTokenKind Kind
        {
            get { return kind; }
        }

        public string Value
        {
            get { return value; }
        }

        public int StartPos
        {
            get { return startPos; }
        }

        public int EndPos
        {
            get { return endPos; }
        }

        public override string ToString()
        {
            return Kind == FffTokenKind.EOL
                ? "<EOL>"
                : Value;
        }

        public bool ContainsCursor(int cursorPos)
        {
            return (StartPos <= cursorPos) && (cursorPos <= EndPos);
        }
    }

    public enum FffTokenKind
    {
        BooleanOperator,
        FieldOperator,
        CollectionOperator,
        CollectionStart,
        CollectionEnd,
        Text,
        EOL,
        Artificial // Used to fill in missing tokens
    }
}