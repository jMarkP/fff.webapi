namespace FFF.WebApi.Core.Compile
{
    public enum FffAnnotatedTokenKind
    {
        Punctuation,
        ReservedWord,
        PropertyName,
        PropertyValue
    }
}