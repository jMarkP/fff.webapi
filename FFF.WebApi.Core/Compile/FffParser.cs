﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFF.WebApi.Core.Clauses;
using FFF.WebApi.Core.Operators;

namespace FFF.WebApi.Core.Compile
{
    public class FffParser<TItem>
    {
        private FffScanner scanner;
        
        public List<FffClause<TItem>> ParseQuery(string input)
        {
            scanner = new FffScanner(input, true, true);
            var result = new List<FffClause<TItem>>();

            result.Add(ParseTerm());
            while (NextTokenIsNotEOL())
            {
                result.Add(ParseTerm());
            }
            return result;
        }

        private FffClause<TItem> ParseTerm()
        {
            var clause = ParseTermInner();
            if (NextTokenIsNotEOL())
            {
                var booleanOperator = scanner.Next();
                if (booleanOperator.Kind != FffTokenKind.BooleanOperator)
                    throw new FffCompileException("Expected 'AND' or '&'");
            }
            return clause;
        }

        private FffClause<TItem> ParseTermInner()
        {
            var firstOperand = scanner.Next();
            if (firstOperand.Kind != FffTokenKind.Text)
                throw new FffCompileException("Expected text");

            // If the next token looks like an operator or collection
            // operator then this is a binary term. Otherwise it's
            // a unary one
            var lookahead = scanner.Lookahead();
            if (lookahead.Kind == FffTokenKind.FieldOperator
                || lookahead.Kind == FffTokenKind.CollectionOperator)
            {
                var op = scanner.Next();
                var isNot = op.Value.Equals("NOT", StringComparison.InvariantCultureIgnoreCase);
                if (isNot)
                {
                    op = scanner.Next();
                }
                // Is this a collection?
                if (op.Value.Equals("IN", StringComparison.InvariantCultureIgnoreCase))
                {
                    // Not supported at the moment
                    throw new NotImplementedException();
                }
                else
                {
                    var secondOperand = scanner.Next();
                    return new FffSinglePropertyClause<TItem>(firstOperand, op, secondOperand);
                }
            }
            else
            {
                return new FffFreeTextClause<TItem>(firstOperand);
            }
        } 

        private bool NextTokenIsNotEOL()
        {
            var token = scanner.Lookahead();
            return token.Kind != FffTokenKind.EOL;
        }
    }

    public enum FffParseState
    {
        FieldOrValue,
        InnerOperator,
        OuterOperator,
        Value,
        Other
    }

}
