using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace FFF.WebApi.Core.Compile
{
    public class FffFilterProperty
    {
        private readonly string path;
        private readonly string displayName;
        private readonly Func<object, IEnumerable<string>> getValue;

        public FffFilterProperty(Type sourceType, string path)
        {
            this.path = path;
            this.displayName = path;
            this.getValue = BuildGetter(sourceType, path);
        }

        public string Path
        {
            get { return path; }
        }

        public string DisplayName
        {
            get { return displayName; }
        }

        public IEnumerable<string> GetValue(object source)
        {
            return getValue(source);
        }

        private Func<object, IEnumerable<string>> BuildGetter(Type sourceType, string propertyName)
        {
            var es = new List<Expression>();
            var vs = new List<ParameterExpression>();

            var source = Expression.Parameter(typeof (object), "source");

            var property = sourceType.GetProperty(propertyName,
                BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic);

            if (property == null)
            {
                throw new FffCompileException(string.Format("No property called {0} found", propertyName));
            }

            // var rawSource = ((TSource)source).Property;
            var rawSource = Expression.Parameter(property.PropertyType, "rawSource");
            vs.Add(rawSource);
            es.Add(Expression.Assign(
                rawSource,
                Expression.Property(
                    Expression.TypeAs(source, sourceType),
                    propertyName)));

            // Either the property is an IEnumerable<something>, or it's not

            Expression getValues = null;

            // TODO
            var isEnumerable = false;
            if (isEnumerable)
            {
                
            }
            else
            {
                var getValuesMethod = this.GetType()
                    .GetMethod("GetValues", BindingFlags.Static | BindingFlags.NonPublic, null, 
                        new Type[] {typeof (object)}, null);
                getValues = Expression.Call(
                    getValuesMethod,
                    Expression.Convert(
                        rawSource,
                        typeof(object))
                    );
            }

            es.Add(getValues);
            
            var lambda = Expression.Lambda<Func<object, IEnumerable<string>>>(
                Expression.Block(vs, es),
                source);

            return lambda.Compile();
        }

        private static IEnumerable<string> GetValues<T>(IEnumerable<T> items)
        {
            return items.Where(x => x != null).Select(item => item.ToString()).ToArray();
        }

        private static IEnumerable<string> GetValues(object item)
        {
            if (item == null)
            {
                return new string[0];
            }
            return new[] {item.ToString()};
        } 
    }

    public static class FffFilterPropertiesCache
    {
        private static Dictionary<Type, FffFilterProperty[]> cache;

        static FffFilterPropertiesCache()
        {
            cache = new Dictionary<Type, FffFilterProperty[]>();
        }

        public static FffFilterProperty[] GetProperties(Type targetType)
        {
            return GetProperties(targetType, "");
        }

        private static FffFilterProperty[] GetProperties(Type targetType, string path)
        {
            if (cache.ContainsKey(targetType))
                return cache[targetType];

            var result = new List<FffFilterProperty>();

            foreach (var property in targetType.GetProperties())
            {
                result.Add(new FffFilterProperty(targetType, path + property.Name));
                // TODO: Recurse
            }

            var properties = result.ToArray();
            cache[targetType] = properties;
            return properties;
        }
    }
}