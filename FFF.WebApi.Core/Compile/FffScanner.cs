using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace FFF.WebApi.Core.Compile
{
    internal interface IFffScanner
    {
        FffToken Next();
        FffToken Lookahead();
    }

    internal class FffScanner : IFffScanner
    {
        private readonly string input;
        private readonly bool ignoreWhitespace;
        private readonly bool stripQuotes;
        private int tokenPos = 0;

        private readonly FffToken[] tokens;

        public FffScanner(string input, bool ignoreWhitespace, bool stripQuotes)
        {
            this.input = input ?? string.Empty;
            this.ignoreWhitespace = ignoreWhitespace;
            this.stripQuotes = stripQuotes;

            this.tokens = Split().ToArray();
        }


        public FffToken Next()
        {
            if (tokenPos >= tokens.Length)
                tokenPos = tokens.Length - 1;
            var token = tokens[tokenPos];
            tokenPos++;
            return token;
        }

        public FffToken Lookahead()
        {
            if (tokenPos >= tokens.Length)
                tokenPos = tokens.Length - 1;
            var token = tokens[tokenPos];
            return token;
        }

        /// <summary>
        /// These characters always begin a new token
        /// </summary>
        private static readonly HashSet<char> reservedChars = new HashSet<char>(new[]
        {
            '!', '=', '~', '(', ')', '"', '\''
        });

        private enum CharacterClass
        {
            BooleanOperator,
            FieldOperator,
            CollectionStart,
            CollectionEnd,
            Quote,
            Text
        }

        private static readonly Dictionary<char, CharacterClass> characterClass = new Dictionary<char, CharacterClass>
        {
            { '&', CharacterClass.BooleanOperator },
            { '!', CharacterClass.FieldOperator },
            { '=', CharacterClass.FieldOperator },
            { '~', CharacterClass.FieldOperator },
            { '(', CharacterClass.CollectionStart },
            { ')', CharacterClass.CollectionEnd },
            { '"',  CharacterClass.Quote},
            { '\'', CharacterClass.Quote }
        };

        private IEnumerable<FffToken> Split()
        {
            var split = new List<FffToken>();
            if (input.Length == 0)
            {
                split.Add(new FffToken(FffTokenKind.EOL, null, 0, 0));
                return split;
            }


            int startOfCurrentToken = 0;
            int pos = 0;
            
            CharacterClass previousCharacterClass = CharacterClass.Text;
            bool previousWasWhitespace = false;
            bool previousWasQuote = false;
            char quoteChar = '"';

            for (pos = 0; pos < input.Length; pos++)
            {
                CharacterClass currentCharacterClass;
                if (!characterClass.TryGetValue(input[pos], out currentCharacterClass))
                {
                    currentCharacterClass = CharacterClass.Text;
                }
                var currentIsWhitespace = char.IsWhiteSpace(input[pos]);

                if (currentCharacterClass == CharacterClass.Quote)
                {
                    // Close off the previous run
                    if ((pos - startOfCurrentToken) > 0
                        && !(previousWasWhitespace && ignoreWhitespace))
                    {
                        split.Add(ConsumeToken(startOfCurrentToken, pos));
                    }
                    // Keep going until we find a matching quote
                    quoteChar = input[pos];
                    var startPos = pos;
                    pos++;
                    while (pos < input.Length && input[pos] != quoteChar)
                    {
                        pos++;
                    }
                    split.Add(ConsumeToken(startPos, pos == input.Length ? pos : pos+1));
                    startOfCurrentToken = pos+1;
                    previousWasQuote = true;
                }
                else
                {
                    // Not in quotes
                    // Are we at a boundary between char types?
                    if (pos > 0 
                        && !previousWasQuote
                        &&
                        (previousCharacterClass != currentCharacterClass
                        || previousWasWhitespace != currentIsWhitespace))
                    {
                        // Yes, consume the previous block of text
                        if (!(previousWasWhitespace && ignoreWhitespace))
                        {
                            split.Add(ConsumeToken(startOfCurrentToken, pos));
                        }
                        startOfCurrentToken = pos;
                    }

                    previousWasWhitespace = currentIsWhitespace;
                    previousCharacterClass = currentCharacterClass;
                    previousWasQuote = false;
                }
            }
            // Don't forget to consume the last token!
            if (startOfCurrentToken < input.Length
                && !(previousWasWhitespace && ignoreWhitespace))
            {
                split.Add(ConsumeToken(startOfCurrentToken, input.Length));
            }
            split.Add(new FffToken(FffTokenKind.EOL, null, input.Length, input.Length));
            return split;
        }

        /// <summary>
        /// Consume the characters from startPos up to one before endPos.
        /// 
        /// e.g. ['a', 'b', 'c', 'd', 'e']
        /// Consume (1, 3) => 'bc'
        /// Consume (0, 5) => 'abcde'
        /// Consume (1, 1) => ''
        /// </summary>
        /// <param name="startPos"></param>
        /// <param name="endPos"></param>
        /// <returns></returns>
        private FffToken ConsumeToken(int startPos, int endPos)
        {
            // Adjust for quotes if required
            if (stripQuotes)
            {
                if (input[startPos] == '"' || input[startPos] == '\'')
                {
                    startPos++;
                }
                if (input[endPos - 1] == '"' || input[endPos - 1] == '\'')
                {
                    endPos--;
                }
            }
            var tokenText = input.Substring(startPos, endPos - startPos);
            return new FffToken(tokenClasses.First(tc => tc.Recognise(tokenText)).Kind, tokenText, startPos, endPos);
        }

        private readonly TokenClass[] tokenClasses = new[]
        {
            new TokenClass(FffTokenKind.BooleanOperator, false, "AND", "&"),
            new TokenClass(FffTokenKind.FieldOperator, false, "=", "!=", "~", "!~"),
            new TokenClass(FffTokenKind.CollectionOperator, false, "not", "in"),
            new TokenClass(FffTokenKind.CollectionStart, false, "("),
            new TokenClass(FffTokenKind.CollectionEnd, false, ")"),
            new TokenClass(FffTokenKind.Text, true),
        };

        private class TokenClass
        {
            private readonly FffTokenKind kind;
            private readonly string[] values;
            private readonly bool acceptsAny;

            public TokenClass(FffTokenKind kind, bool acceptsAny, params string[] values)
            {
                this.kind = kind;
                this.values = values;
                this.acceptsAny = acceptsAny;
            }

            public FffTokenKind Kind
            {
                get { return kind; }
            }

            public bool Recognise(string target)
            {
                return acceptsAny || values.Any(s => s.Equals(target, StringComparison.InvariantCultureIgnoreCase));
            }
        }
    }




    //internal class FffScanner : IFffScanner
    //{
    //    private readonly string input;
    //    private int pos = 0;
    //    private List<FffToken> tokens;
    //    private int tokenPos = 0;

    //    public FffScanner(string input)
    //    {
    //        this.input = input ?? "";
    //        tokens = new List<FffToken>();
    //        var token = GetNext(true);
    //        while (token.Kind != FffTokenKind.EOL)
    //        {
    //            tokens.Add(token);
    //            token = GetNext(true);
    //        }
    //        tokens.Add(token);
    //    }

    //    public FffToken Next()
    //    {
    //        if (tokenPos >= tokens.Count)
    //            tokenPos = tokens.Count - 1;
    //        var token = tokens[tokenPos];
    //        tokenPos++;
    //        return token;
    //    }

    //    public FffToken Lookahead()
    //    {
    //        if (tokenPos >= tokens.Count)
    //            tokenPos = tokens.Count - 1;
    //        var token = tokens[tokenPos];
    //        return token;
    //    }

    //    private FffToken GetNext(bool consume)
    //    {
    //        FffToken result;
    //        var originalPos = pos;

    //        EatWhitespace();

    //        // Are we at the end
    //        if (EOL)
    //        {
    //            return new FffToken(FffTokenKind.EOL, null);
    //        }


    //        // What type of character is it?
    //        // At the moment, just operator, punctuation or text
    //        if (punctuationChars.Contains(Current.Value))
    //        {
    //            result = new FffToken(FffTokenKind.Text, EatOne().ToString());
    //        }
    //        else
    //        {
    //            var isOperatorChar = IsOperatorChar(Current.Value);
    //            var text = EatWhile(x => 
    //                !IsPunctuationCharacter(x)
    //                && !IsWhitespace(x) 
    //                && (isOperatorChar
    //                    ? IsOperatorChar(x)
    //                    : !IsOperatorChar(x)));

    //            result = new FffToken(FffTokenKind.Text, text);
    //        }

    //        if (!consume)
    //            pos = originalPos;
    //        return result;
    //    }

    //    private bool IsPunctuationCharacter(char c)
    //    {
    //        return punctuationChars.Contains(c);
    //    }

    //    private bool IsOperatorChar(char c)
    //    {
    //        return operatorStartChars.Contains(c);
    //    }

    //    private bool IsWhitespace(char c)
    //    {
    //        return char.IsWhiteSpace(c);
    //    }

    //    private void EatWhitespace()
    //    {
    //        while (!EOL && char.IsWhiteSpace(Current.Value))
    //        {
    //            Advance();
    //        }
    //    }

    //    private char EatOne()
    //    {
    //        Debug.Assert(!EOL);
    //        var c = Current;
    //        Advance();
    //        return c.Value;
    //    }

    //    private string EatWhile(Func<char, bool> condition)
    //    {
    //        int startPos = pos;
    //        int length = 0;
    //        while (!EOL && condition(Current.Value))
    //        {
    //            length++;
    //            Advance();
    //        }
    //        return input.Substring(startPos, length);
    //    }

    //    private char? Current
    //    {
    //        get
    //        {
    //            return pos < input.Length
    //                ? input[pos]
    //                : (char?) null;
    //        }
    //    }

    //    private bool EOL
    //    {
    //        get { return pos == input.Length; }
    //    }

    //    private void Advance()
    //    {
    //        pos++;
    //    }

    //    private static readonly char[] punctuationChars = new []
    //    {
    //        '[',
    //        ']',
    //        ','
    //    };

    //    private static readonly char[] operatorStartChars = new[]
    //    {
    //        '=',
    //        '~',
    //        '!', // != | !~
    //    };
    //}
}