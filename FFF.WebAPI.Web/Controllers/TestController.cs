﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FFF.WebApi.Core;
using FFF.WebApi.Core.Compile;

namespace FFF.WebAPI.Web.Controllers
{
    public class TestController : ApiController
    {
        [HttpGet]
        [Route("test")]
        public IEnumerable<TestDto> GetFiltered(string filterText)
        {
            var unfiltered = Enumerable.Range(0, 200000)
                .Select(i => new TestDto
                {
                    Count = i,
                    Flag = (i%6) == 0,
                    Name = "Test" + i,
                    PossibleFlag = (i%8) == 0 ? (bool?) null : (i%4) == 0
                });

            var compiled = new FffParser<TestDto>().ParseQuery(filterText);
            var filter = new FffCompiledFilter<TestDto>(compiled);
            return filter.Filter(unfiltered);
        }

        [Route("test/suggest")]
        public IEnumerable<FffSuggestion> GetSuggestions(string filterText, int cursorPos)
        {
            var unfiltered = Enumerable.Range(0, 200000)
                .Select(i => new TestDto
                {
                    Count = i,
                    Flag = (i % 6) == 0,
                    Name = "Test" + i,
                    PossibleFlag = (i % 8) == 0 ? (bool?)null : (i % 4) == 0
                });

            var generator = new FffManager();
            return generator.GetSuggestions(filterText, cursorPos, 100, unfiltered);
        }

    }

    public class TestDto
    {
        public string Name { get; set; }
        public bool Flag { get; set; }
        public int Count { get; set; }
        public bool? PossibleFlag { get; set; }
    }
}
